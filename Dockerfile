FROM python:3.8.19
LABEL authors="admin"

WORKDIR /opt/heart_center

COPY . .

ENV DATABASES_USERNAME="root"
ENV DATABASES_PASSWORD="mongo_KbK3PS"
ENV DATABASES_HOST="122.51.121.230"
ENV DATABASES_PORT=27017
ENV DATABASES_NAME="heart"
ENV DATABASES_AUTHSource="admin"
ENV X_API_KEY="f0267098dbb8486086a536b1bd350d0a.62ff226183129a73"

RUN pip config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple
RUN pip install -U pip setuptools && pip install poetry
RUN poetry install

CMD [ "poetry", "run", "uvicorn", "main:app","--host", "0.0.0.0" ,"--port" ,"8000", "--reload" ]
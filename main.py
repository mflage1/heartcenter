# -*- coding: utf-8 -*-
import uvicorn
from fastapi import FastAPI
from routers import api
from fastapi.staticfiles import StaticFiles
from fastapi.responses import FileResponse
from fastapi.middleware.cors import CORSMiddleware
from fastapi_cdn_host import patch_docs

app = FastAPI()
patch_docs(app)

app.include_router(api.router)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.mount("/static", StaticFiles(directory="static"), name="static")


@app.get("/")
async def index():
    return FileResponse('template/index.html')


if __name__ == '__main__':
    uvicorn.run(app, port=8001)

# -*- coding: utf-8 -*-
"""
File Name：     parse
Description :
date：          2024/2/22 022
"""
import json
import time
import pymongo
import pendulum
from datetime import datetime
from bson import ObjectId
from fastapi import HTTPException
from util.message import send_message, search_user, search_company, check_invoices
from settings import DATABASES_NAME, DATABASES_PASSWORD, DATABASES_PORT, DATABASES_USERNAME, DATABASES_HOST, \
    DATABASES_AUTHSource
from util.calc import example_score

mongo_client = pymongo.MongoClient(
    f"mongodb://{DATABASES_USERNAME}:{DATABASES_PASSWORD}@{DATABASES_HOST}:{DATABASES_PORT}/?"
    f"authSource={DATABASES_AUTHSource}")
# mongo_client = pymongo.MongoClient(
#     f"mongodb+srv://{DATABASES_USERNAME}:{DATABASES_PASSWORD}@{DATABASES_HOST}/?retryWrites=true&"
#     f"w=majority&appName={DATABASES_AUTHSource}")
mongo_db = mongo_client[DATABASES_NAME]
heart_texts = mongo_db['texts']
heart_forms = mongo_db['forms']
heart_datas = mongo_db['datas']
heart_weight = mongo_db['weight']
heart_users = mongo_db['users']
heart_company = mongo_db['company']
heart_save = mongo_db['save']

heart_first_forms = mongo_db['first_forms']

heart_orders = mongo_db['orders']


def sub_calc(project_type, user_id, old_project_type):
    status = False
    try:
        orders_many = heart_orders.find({"user_id": user_id, 'type': 'general'})
        for order in orders_many:
            print(order)
            if order['invoice_url']:
                invoice_id = order['invoice_url'].split('/')[-1]
                check_in, order_info = check_invoices(invoice_id)
                if check_in:
                    heart_orders.update_one({"_id": ObjectId(order["_id"])}, {"$set": {"status": check_in}})
                    order['status'] = check_in
            if order['status'] == 'paid' and order[f'use_{project_type}'] > 0:
                order[f'use_{project_type}'] -= 1
                heart_orders.update_one({"_id": ObjectId(order["_id"])}, {
                    "$set": {f'use_{project_type}': order[f'use_{project_type}']}})
                if old_project_type is not None:
                    order[f'use_{old_project_type}'] += 1
                    heart_orders.update_one({"_id": ObjectId(order["_id"])}, {
                        "$set": {f'use_{project_type}': order[f'use_{project_type}'],
                                 f'use_{old_project_type}': order[f'use_{old_project_type}']}})
                status = True
                break
        print(status)
        return status
    except Exception as e:
        print(e)
        raise HTTPException(500, "No subscriptions available")
        # raise HTTPException(500, "No subscriptions available")


def get_users(user_id: str):
    user = heart_users.find_one({'user_id': user_id}, {'_id': 0})
    company = None
    if not user:
        user = search_user(user_id)
        user = user if type(user) != bool else None
        if user:
            user['user_id'] = user['id']
            user.pop('id')
            heart_users.insert_one(user)
        else:
            raise ValueError("用户不存在！，或key余额不足、失效")
    companyId = user['companyId']
    if user and companyId:
        company = heart_company.find_one({'company_id': companyId}, {'_id': 0})
        if not company:
            company = search_company(companyId)
            company = company if type(company) != bool else None
            if company:
                company['company_id'] = company['id']
                company.pop('id')
                heart_company.insert_one(company)
            else:
                raise ValueError("公司不存在！，或key余额不足、失效")
    if user and company:
        return {'user': user, 'company': company}


def custom_json_encoder(obj):
    """
    转换mongo对象为json
    :param obj:
    :return:
    """

    def paser_json(data):
        for k, v in data.items():
            if isinstance(v, ObjectId):
                data[k] = str(v)
            elif isinstance(v, datetime):
                data[k] = v.strftime('%Y-%m-%d %H:%M:%S')
            elif isinstance(v, dict):
                data[k] = paser_json(v)
        return data

    res = []
    for data in obj:
        data['texts'] = data['texts'][0] if data.get('texts') else None
        data['forms'] = data['forms'][0] if data.get('forms') else None
        data['datas'] = data['datas'][0] if data.get('datas') else None
        data['first_forms'] = data['first_forms'][0] if data.get('first_forms') else None
        res.append(paser_json(data))
    return res


def parse_text_to_form(text: str, user_id: str, internal_id):
    """
    解析用户上传文本为问卷数据
    :param internal_id:
    :param text:
    :param user_id:
    :return:
    """

    heart_first_forms_source = heart_first_forms.find_one({"_id": ObjectId(internal_id)},
                                                          {'forms': 1, 'project_type': 1})
    project_type = heart_first_forms_source['project_type']

    # sub_calc(project_type, user_id)

    heart_texts_tmp = heart_texts.find_one({'_id': ObjectId(internal_id)}, {'text': 1})
    heart_texts_source = heart_texts_tmp.get('text') if heart_texts_tmp and heart_texts_tmp.get('text') else []

    """重复提交文件"""
    heart_first_forms_sourcett = heart_first_forms.find_one({"_id": ObjectId(internal_id)}, {'forms': 1})
    heart_first_forms_sourcett = heart_first_forms_sourcett.get(
        'forms') if heart_first_forms_sourcett and heart_first_forms_sourcett.get(
        'forms') else []

    heart_texts_sourcett = heart_texts.find_one({"_id": ObjectId(internal_id)}, {'text': 1})
    heart_texts_sourcett = heart_texts_sourcett.get('text') if heart_texts_sourcett.get('text') else []

    # if heart_first_forms_sourcett and heart_texts_sourcett:
    if len(heart_first_forms_sourcett) < len(heart_texts_sourcett):
        heart_texts_source.pop()

    heart_texts_source.append(text)
    text_insert_result = {
        'user_id': user_id,
        'text': heart_texts_source,
        'uploadTime': pendulum.now()
    }
    internal_id = ObjectId(internal_id)
    text_inst_id = heart_texts.update_one({'_id': internal_id}, {'$set': text_insert_result}, upsert=True)
    heart_forms.update_one({"_id": ObjectId(internal_id)}, {"$set": {"uploadTime": None}})
    heart_datas.update_one({"_id": ObjectId(internal_id)}, {"$set": {"uploadTime": None}})

    heart_texts.update_one({"_id": internal_id},
                           {"$set": {"completionTime": pendulum.now()}})

    '''parse_result为模拟解析文本后结果，线上需替换'''
    get_users(user_id)
    """使用get_users获取用户详细信息
    {'user': 
        {'avatarImageUrl': 'https://lh3.googleusercontent.com/a/ACg8ocIBCSnYv5PNAxnrIFbuTjytZlgLObj9-ORrA9ffcxaxhQ=s96-c', 
        ...
        'email': 'mflage0@gmail.com', 
        'familyName': 'test',  
        ...
        'user_id': '43443c7b-0141-40c0-9652-8f5467ab2cf5'}, 
    'company': 
        {'object': 'company', 
        'createdAt': '2024-02-22T03:29:59.935279437Z', 
        'name': '', 
        'fallbackColor': '#EC4E20', 
        'iconImageUrl': '', 
        'isPlaceholder': True, 
        'company_id': '1b392a2c-a4a1-4e35-a737-84ec388ffc3f'}}
    """
    parse_result = [
        {
            "question_type": "i",
            "question_content": "Revenue Source",
            "extacted_values": [
                [
                    "Contracted RECs - Duke Energy Carolinas",
                    "$225,019"
                ],
                [
                    "Contracted RECs - NC EMC",
                    "$1,125,000"
                ]
            ]
        },
        {
            "question_type": "m",
            "question_content": "Total Capital Expense (CAPEX)",
            "extacted_values": [
                "$43,173,784",
                "$47,172,589",
                "None of the above"
            ]
        },
        {
            "question_type": "m",
            "question_content": "Total Operating Expense (OPEX)",
            "extacted_values": [
                "$2,098,673",
                "$3,091,423",
                "None of the above"
            ]
        },
        {
            "question_type": "f",
            "question_content": "Power Purchase Agreement"
        },
        {
            "question_type": "f",
            "question_content": "Carbon Credit Agreement"
        },
        {
            "question_type": "f",
            "question_content": "Other Revenue Agreements"
        },
        {
            "question_type": "f",
            "question_content": "Engineering, Procurement and Construction Agreement (EPC)"
        },
        {
            "question_type": "f",
            "question_content": "Operations and Maintenance Agreement (O&M)"
        },
        {
            "question_type": "f",
            "question_content": "Other Vendor or Expense Agreement"
        },
        {
            "question_type": "f",
            "question_content": "Indirect Costs Agreement"
        },
        {
            "question_type": "f",
            "question_content": "Feedstock or Fuel Agreement"
        },
        {
            "question_type": "b",
            "question_content": "Feedstock (or Fuel) Cost",
            "extacted_values": "$2,286,374"
        }
    ]
    forms_insert_result = {
        '_id': internal_id,
        'user_id': user_id,
        'forms': parse_result,
        'uploadTime': pendulum.now()
    }
    forms_insert_id = heart_forms.update_one({'_id': internal_id}, {'$set': forms_insert_result}, upsert=True)

    heart_first_forms_source = heart_first_forms.find_one({"_id": ObjectId(internal_id)}, {'forms': 1})
    heart_first_forms_source = heart_first_forms_source.get(
        'forms') if heart_first_forms_source and heart_first_forms_source.get(
        'forms') else []

    heart_texts_source = heart_texts.find_one({"_id": ObjectId(internal_id)}, {'text': 1})
    heart_texts_source = heart_texts_source.get('text') if heart_texts_source.get('text') else []

    if heart_texts_source and heart_first_forms_source:
        if len(heart_first_forms_source) == len(heart_texts_source):
            """^判断表单是否填充完全"""
            if 'last_time' not in heart_datas.find_one({"_id": ObjectId(internal_id)}, {}):
                if not sub_calc(project_type, user_id, None):
                    raise HTTPException(status_code=400, detail="No subscriptions available")
            heart_datas.update_one({'_id': ObjectId(internal_id)},
                                   {'$set': {'completionTime': pendulum.now(),
                                             'last_time': pendulum.now()}}, upsert=True)
            send_message(member_ship_entity_id=user_id,
                         text=f"{str(internal_id)}\nYou will recieve the score dashboard shortly!"
                              f"Please click the link to view it.\nhttps://portal.ceartscore.com/apps?id=e14a545e-a0cf-4e51-8a35-de0fd238a56b")


def parse_form_to_datas(form: list, user_id: str, internal_id: str, inside: bool = False):
    """
    用户调整表单后解析分数
    :param inside: 。。。
    :param internal_id: 内部数据id
    :param form: 表单数据
    :param user_id: 用户id
    :return:
    """
    heart_forms.update_one({'_id': ObjectId(internal_id), 'user_id': user_id},
                           {'$set': {'forms': form, 'updateTime': pendulum.now()}})
    if not inside:
        with open("log.txt", mode="w", encoding='utf-8') as log_file:
            content = f"{json.dumps(form, ensure_ascii=False)}"
            log_file.write(user_id)
            log_file.write(content)
        heart_forms.update_one({"_id": ObjectId(internal_id)},
                               {"$set": {"completionTime": pendulum.now()}})
        heart_datas.update_one({"_id": ObjectId(internal_id)}, {"$set": {"uploadTime": None}})
        time.sleep(10)
        '''m_data为模拟解析文本后结果，线上需替换'''
        m_data = example_score(1, 1)
        parse_result = {'scores': m_data[0], 'weighted_score': m_data[-1][0], 'history_score': []}
        #####################################
        datas_insert_result = {
            'user_id': user_id,
            'datas': parse_result,
            'uploadTime': pendulum.now(),
            'completionTime': pendulum.now(),
            'custom': False,
            'last_time': pendulum.now(),
        }
        heart_datas_search = heart_datas.find_one({'_id': ObjectId(internal_id)})
        parse_result['history_score'] = heart_datas_search['datas'][
            'history_score'] if heart_datas_search else [
            {pendulum.now().to_date_string(): parse_result['weighted_score']}]
        parse_result['history_score'] += [
            {pendulum.now().to_date_string(): parse_result['weighted_score']}] if heart_datas_search else []
        heart_datas.update_one({'_id': ObjectId(internal_id)},
                               {'$set': datas_insert_result}, upsert=True)

        send_message(member_ship_entity_id=user_id,
                     text=f"{str(internal_id)}\nYour survey progress has been updated. "
                          f"Please click the link to view it.\nhttps://portal.ceartscore.com/apps?id=e14a545e-a0cf-4e51-8a35-de0fd238a56b")


def first_parse_form_to_datas(form: list, user_id: str, internal_id: str, inside: bool = False):
    """
    用户调整表单后解析分数
    :param inside: 。。。
    :param internal_id: 内部数据id
    :param form: 表单数据
    :param user_id: 用户id
    :return:
    """

    heart_first_forms_source = heart_first_forms.find_one({"_id": ObjectId(internal_id)},
                                                          {'forms': 1, 'project_type': 1})
    project_type = heart_first_forms_source['project_type']

    # sub_calc(project_type, user_id)

    heart_first_forms_tmp = heart_first_forms.find_one({'_id': ObjectId(internal_id)}, {'forms': 1})
    heart_first_forms_source = heart_first_forms_tmp.get(
        'forms') if heart_first_forms_tmp and heart_first_forms_tmp.get('forms') else []
    if heart_first_forms_source:
        if heart_first_forms_source[-1][0]['save']:
            heart_first_forms_source.pop()
    heart_first_forms_source.append(form)
    heart_first_forms.update_one({'_id': ObjectId(internal_id), 'user_id': user_id},
                                 {'$set': {'forms': heart_first_forms_source,
                                           'updateTime': pendulum.now()}}, upsert=True)
    if not inside:
        heart_first_forms.update_one({"_id": ObjectId(internal_id)},
                                     {"$set": {"completionTime": pendulum.now()}}, upsert=True)
        heart_datas.update_one({"_id": ObjectId(internal_id)}, {"$set": {"uploadTime": None}})
        '''m_data为模拟解析文本后结果，线上需替换'''
        m_data = example_score(1, 1)
        parse_result = {'scores': m_data[0], 'weighted_score': m_data[-1][0], 'history_score': []}
        #####################################
        datas_insert_result = {
            'user_id': user_id,
            'datas': parse_result,
            'uploadTime': pendulum.now(),
            'completionTime': None,
            'custom': False
        }
        heart_datas_search = heart_datas.find_one({'_id': ObjectId(internal_id)})
        parse_result['history_score'] = heart_datas_search['datas'][
            'history_score'] if heart_datas_search.get('datas') else [
            {pendulum.now().to_date_string(): parse_result['weighted_score']}]
        print(parse_result['history_score'])
        parse_result['history_score'] += [
            {pendulum.now().to_date_string(): parse_result['weighted_score']}] if heart_datas_search.get('datas') else []
        print(parse_result['history_score'])
        heart_datas.update_one({'_id': ObjectId(internal_id)},
                               {'$set': datas_insert_result}, upsert=True)

        heart_first_forms_source = heart_first_forms.find_one({"_id": ObjectId(internal_id)},
                                                              {'forms': 1})
        heart_first_forms_source = heart_first_forms_source.get(
            'forms') if heart_first_forms_source and heart_first_forms_source.get(
            'forms') else []

        heart_texts_source = heart_texts.find_one({"_id": ObjectId(internal_id)}, {'text': 1})
        heart_texts_source = heart_texts_source.get('text') if heart_texts_source.get('text') else []
        if heart_texts_source and heart_first_forms_source:
            if len(heart_first_forms_source) == len(heart_texts_source):
                """^判断文件是否上传完成"""
                if 'last_time' not in heart_datas.find_one({"_id": ObjectId(internal_id)}, {}):
                    if not sub_calc(project_type, user_id, None):
                        raise HTTPException(status_code=400, detail="No subscriptions available")
                heart_datas.update_one({'_id': ObjectId(internal_id)},
                                       {'$set': {'completionTime': pendulum.now(),
                                                 'last_time': pendulum.now()}}, upsert=True)
                send_message(member_ship_entity_id=user_id,
                             text=f"{str(internal_id)}\nYour survey progress has been updated. "
                                  f"Please click the link to view it.\nhttps://portal.ceartscore.com/apps?id=e14a545e-a0cf-4e51-8a35-de0fd238a56b")

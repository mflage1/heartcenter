# -*- coding: utf-8 -*-
"""
File Name：     calc
Description :
date：          2024/5/20 020
"""
import numpy as np


def example_score(ClientID, ProjectID):
    scores = {}
    scores['site and interconnection'] = 89
    scores['revenue'] = 74
    scores['sponsor and counterparty'] = 73
    scores['proforma quality'] = 71
    scores['construction risk'] = 68
    scores['performance risk'] = 59
    scores['esg'] = 48
    scores['doc set quality'] = 41
    scores['contract bankability'] = 38
    scores['capital stack'] = 29
    return scores, calculate_weighted_score(scores)


def calculate_weighted_score(scores, weights=None):
    score_values = np.array(list(scores.values()))
    if weights is None:
        weights = np.ones_like(score_values)
    return np.sum(score_values * weights) / np.sum(weights)


# -*- coding: utf-8 -*-
"""
File Name：     qst
Description :
date：          2024/7/31 031
"""


def test_ret():
    #################################################################################
    #
    # keys: question_type, question_content, extacted_values
    #
    # question_type
    # 'm' = 'multi-choice'
    # 'f' = 'file upload'
    # 'b' = 'blank with contents'
    # 'm-m' = 'multi-choice, multi-answer'
    # 'm-f' = 'multi-choice, f associated with certain options'
    # 'i-f' = 'insert item with a file upload option'
    # 'b-d' = 'The answer needs to be choose from a calender'
    #
    #################################################################################
    example = [{'question_type': 'm', 'question_content': 'At what stage of development do you consider the project?',
                'choices': [
                    'Early Stage (pre-execution of all or most Foundational Documents such as Site control, PPA, Interconnection, REC/Carbon Credit or other revenue, Feedstock)',
                    'Advanced Development (most foundational documents executed, critical permits issued)',
                    'Funding Term Sheet negotiated or executed', 'Post Term Sheet Due Diligence Period',
                    'Financial Close (for at least Construction Funding', 'Under Construction',
                    'COD/Final Financial Close']},
               {'question_type': 'm-f', 'question_content': 'What is the status of the Erosion Control Permit?',
                'choices': ['Issued', 'Submitted but not Issued', 'Application Underway', 'Application not Started'],
                'f_options': [1, 2, 3], 'file': 'Stormwater Permit/Plan or Application'},
               {'question_type': 'f', 'question_content': 'Proferma'},
               {'question_type': 'b', 'question_content': 'What is the name of the Project Sponsor (developer)?'},
               {'question_type': 'b-d', 'question_content': 'What is the date of Incorporation of the Sponsor?'},
               {'question_type': 'm-m', 'question_content': 'Please select more than more choices.',
                'choices': ['A', 'B', 'C', 'D']},
               {'question_type': 'i-f', 'question_content': 'Revenue Source', 'items': ['Main Revenue Source']}]
    return example


if __name__ == '__main__':
    print(example_score(1, 1)[0])

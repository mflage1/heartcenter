# -*- coding: utf-8 -*-
"""
File Name：     parse
Description :
date：          2024/2/22 022
"""
import json
import time
import pymongo
import pendulum
from datetime import datetime
from bson import ObjectId
from util.message import send_message
from settings import DATABASES_NAME, DATABASES_PASSWORD, DATABASES_PORT, DATABASES_USERNAME, DATABASES_HOST, \
    DATABASES_AUTHSource

mongo_client = pymongo.MongoClient(
    f"mongodb://{DATABASES_USERNAME}:{DATABASES_PASSWORD}@{DATABASES_HOST}:{DATABASES_PORT}/?"
    f"authSource={DATABASES_AUTHSource}")
# mongo_client = pymongo.MongoClient(
#     f"mongodb+srv://{DATABASES_USERNAME}:{DATABASES_PASSWORD}@{DATABASES_HOST}/?retryWrites=true&"
#     f"w=majority&appName={DATABASES_AUTHSource}")
mongo_db = mongo_client[DATABASES_NAME]
heart_texts = mongo_db['texts']
heart_forms = mongo_db['forms']
heart_datas = mongo_db['datas']


def custom_json_encoder(obj):
    """
    转换mongo对象为json
    :param obj:
    :return:
    """

    def paser_json(data):
        for k, v in data.items():
            if isinstance(v, ObjectId):
                data[k] = str(v)
            elif isinstance(v, datetime):
                data[k] = v.strftime('%Y-%m-%d %H:%M:%S')
            elif isinstance(v, dict):
                data[k] = paser_json(v)
        return data

    res = []
    for data in obj:
        data['texts'] = data['texts'][0] if data.get('texts') else None
        data['forms'] = data['forms'][0] if data.get('forms') else None
        data['datas'] = data['datas'][0] if data.get('datas') else None
        res.append(paser_json(data))
    return res


def parse_text_to_form(text: str, user_id: str, internal_id):
    """
    解析用户上传文本为问卷数据
    :param internal_id:
    :param text:
    :param user_id:
    :return:
    """
    text_insert_result = {
        'user_id': user_id,
        'text': text,
        'uploadTime': pendulum.now()
    }
    internal_id = ObjectId(internal_id)
    text_inst_id = heart_texts.update_one({'_id': internal_id}, {'$set': text_insert_result}, upsert=True)
    with open("log.txt", mode="w", encoding='utf-8') as log_file:
        content = f"{text}"
        log_file.write(user_id)
        log_file.write(content)
    time.sleep(10)
    heart_texts.update_one({"_id": internal_id},
                           {"$set": {"completionTime": pendulum.now()}})

    '''parse_result为模拟解析文本后结果，线上需替换'''
    parse_result = [
        {
            "question_type": "i",
            "question_content": "Revenue Source",
            "extacted_values": [
                [
                    "Contracted RECs - Duke Energy Carolinas",
                    "$225,019"
                ],
                [
                    "Contracted RECs - NC EMC",
                    "$1,125,000"
                ]
            ]
        },
        {
            "question_type": "m",
            "question_content": "Total Capital Expense (CAPEX)",
            "extacted_values": [
                "$43,173,784",
                "$47,172,589",
                "None of the above"
            ]
        },
        {
            "question_type": "m",
            "question_content": "Total Operating Expense (OPEX)",
            "extacted_values": [
                "$2,098,673",
                "$3,091,423",
                "None of the above"
            ]
        },
        {
            "question_type": "f",
            "question_content": "Power Purchase Agreement"
        },
        {
            "question_type": "b",
            "question_content": "Feedstock (or Fuel) Cost",
            "extacted_values": "$2,286,374"
        }
    ]
    forms_insert_result = {
        '_id': internal_id,
        'user_id': user_id,
        'forms': parse_result,
        'uploadTime': pendulum.now()
    }
    forms_insert_id = heart_forms.update_one({'_id': internal_id}, {'$set': forms_insert_result}, upsert=True)
    send_message(member_ship_entity_id=user_id,
                 text=f"{str(internal_id)}\nYou will recieve the score dashboard shortly!"
                      f"Please click the link to view it.\nhttps://portal.ceartscore.com/extensions?id=da73a5ed-0655-4996-9e19-d6504f83ab6d")


def parse_form_to_datas(form: list, user_id: str, internal_id: str):
    """
    用户调整表单后解析分数
    :param internal_id: 内部数据id
    :param form: 表单数据
    :param user_id: 用户id
    :return:
    """
    heart_forms.update_one({'_id': ObjectId(internal_id), 'user_id': user_id},
                           {'$set': {'forms': form, 'updateTime': pendulum.now()}})
    with open("log.txt", mode="w", encoding='utf-8') as log_file:
        content = f"{json.dumps(form, ensure_ascii=False)}"
        log_file.write(user_id)
        log_file.write(content)
    time.sleep(10)
    heart_forms.update_one({"_id": ObjectId(internal_id)},
                           {"$set": {"completionTime": pendulum.now()}})

    '''parse_result为模拟解析文本后结果，线上需替换'''
    parse_result = {
        'scores': {
            'site and interconnection': 89,
            'revenue': 74,
            'sponsor and counterparty': 73,
            'proforma quality': 71,
            'construction risk': 68,
            'performance risk': 59,
            'esg': 48,
            'doc set quality': 41,
            'contract bakability': 38,
            'capital stack': 29
        },
        'weighted_score': 59.0,
    }
    #####################################
    parse_result['history_score'] = []
    datas_insert_result = {
        'user_id': user_id,
        'datas': parse_result,
        'uploadTime': pendulum.now(),
        'completionTime': pendulum.now()
    }
    heart_datas_search = heart_datas.find_one({'_id': ObjectId(internal_id)})
    parse_result['history_score'] = heart_datas_search['datas'][
        'history_score'] if heart_datas_search else [{pendulum.now().to_date_string(): parse_result['weighted_score']}]
    parse_result['history_score'] += [
        {pendulum.now().to_date_string(): parse_result['weighted_score']}] if heart_datas_search else []
    heart_datas.update_one({'_id': ObjectId(internal_id)},
                           {'$set': datas_insert_result}, upsert=True)

    send_message(member_ship_entity_id=user_id,
                 text=f"{str(internal_id)}\nYour survey progress has been updated. "
                      f"Please click the link to view it.\nhttps://portal.ceartscore.com/extensions?id=da73a5ed-0655-4996-9e19-d6504f83ab6d")

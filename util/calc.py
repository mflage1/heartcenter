# -*- coding: utf-8 -*-
"""
File Name：     calc
Description :
date：          2024/5/20 020
"""
import numpy as np


def example_score(ClientID, ProjectID):
    scores = {}
    scores['site and interconnection'] = 89
    scores['revenue'] = 74
    scores['sponsor and counterparty'] = 73
    scores['proforma quality'] = 71
    scores['construction risk'] = 68
    scores['performance risk'] = 59
    scores['esg'] = 48
    scores['doc set quality'] = 41
    scores['contract bankability'] = 38
    scores['capital stack'] = 29
    return scores, calculate_weighted_score(scores)


def calculate_weighted_score(scores, weights=None):
    score_values = np.array(list(scores.values()))
    if weights is None:
        weights = np.ones_like(score_values)
    client_stage = 6
    ceart_stage = 4
    return int(np.sum(score_values * weights) / np.sum(weights)), client_stage, ceart_stage


# -*- coding: utf-8 -*-
"""
File Name：     qst
Description :
date：          2024/7/31 031
"""


def test_ret():
    #################################################################################
    #
    # keys: question_type, question_content, extacted_values
    #
    # question_type
    # 'm' = 'multi-choice'
    # 'f' = 'file upload'
    # 'b' = 'blank with contents'
    # 'm-m' = 'multi-choice, multi-answer'
    # 'm-f' = 'multi-choice, f associated with certain options'
    # 'i-f' = 'insert item with a file upload option'
    # 'b-d' = 'The answer needs to be choose from a calender'
    #
    #################################################################################
    example = [
        {
            'section_name': 'Proforma Upload',
            'text': 'The first item to be addressed is the project proforma. This is typically in an excel format. For protection of IP, it is acceptable to save the project proforma with numbers only (no formulas); The CEARTscore™ will contain extra points if the original proforma is uploaded so that it can be scanned for completeness, such as inclusion of revenue and expense numbers for the term of the project, and other best-practices. \n CEARTscore™ requires that a summary proforma in a recognizable format be uploaded to ensure that variables are properly read. You may download this proforma here. To get the maximum amount of points for the proforma, please take this summary sheet and make it the first page of the project proforma, and link the highlighted cells back to the appropriate place in the project proforma. It is possible to proceed without the existing proforma—just with the cover page template filled in rather than linked, but this will incur a penalty in the score. It’s a bit like the math teacher in school asking you to show your work—the numbers have more validity with some justification for them. \n If there is no upload of a proforma, in either format, the CEARTscore™ will be significantly reduced. If the items above are too confusing or time-consuming, CEARTscore™ can provide a human consultant to get the data in the correct format. Click here to explore that option.',
            'questions': []
        },
        {
            'section_name': 'Introduction to CEARTscore™ Survey',
            'text': 'Welcome to the CEARTscore™ on-ramp survey. Below you will find a list of questions about the project, along with the \
                   opportunity to upload documents. This is a bit of a lengthy process, but it is necessary to make sure the platform has all of the \
                   necessary information required to produce an accurate and clear risk score, and a summary report that is helpful for project \
                   improvement or achieving a term sheet or funding milestone. Please allow yourself at least an hour to complete the survey. \
                   You may start the survey and return to it. \n \
                   If a document does not exist for any request, you may just click “Submit” with the upload line blank. \
                   There will be some questions which may seem redundant to uploaded documents and other questions. \
                   CEARTscore™ makes every attempt to triangulate extracted and survey data to ensure accuracy, so a question may be asked \
                   in more ways than one.',
            'questions': []
        },

        {
            'section_name': 'Proforma',
            'text': '',
            'questions': [
                {'question_type': 'm',
                 'question_content': 'Are you adding the CEARTscore™ proforma summary template by itself, or as a sheet added to the front of the original proforma?',
                 'choices': ['CEARTscore™ summary proforma by itself',
                             'CEARTscore™ summary proforma attached as the front/first sheet of the original proforma',
                             'CEARTscore™ summary proforma attached as the front/first sheet of the original proforma and with the critical cells  (highlighted on the sheet) linked back to the relevant sections of the original proforma'],
                 'f_options': [0],
                 'file': 'Upload the original proforma here to earn extra CEARTscore™ credits.'}
            ]
        },

        {
            'section_name': 'General Questions',
            'text': 'Please answer this series of general questions about the project.',
            'questions': [
                {'question_type': 'b', 'question_content': 'What is the name of the Project Sponsor (developer)?'},
                {'question_type': 'b', 'question_content': 'What is the address of the Sponsor?'},
                {'question_type': 'b', 'question_content': 'What is the State of Incorporation of the Sponsor?'},
                {'question_type': 'b-d', 'question_content': 'What is the date of Incorporation of the Sponsor?'},
                {'question_type': 'b',
                 'question_content': 'What is the name of the entity which owns the project(SPV)?'},
                {'question_type': 'b', 'question_content': 'What is the address of the Project entity?'},
                {'question_type': 'b', 'question_content': 'What is the State of Incorporation of the Project?'},
                {'question_type': 'b-d', 'question_content': 'What is the date of Incorporation of the Project?'},
                {'question_type': 'b',
                 'question_content': 'What is the title or name of the project (what does the Sponsor call the project)?'},
                {'question_type': 'b', 'question_content': 'What is the physical address of the project?'},

                {
                    "question_type": "i",
                    "question_content": "Name and Address of secondary or satellite sites which are integral to the project.",
                    "extacted_values": [],
                    'text': 'Please enter the name of the site.',
                    'prefix': 'The address of '
                },

                {'question_type': 'b',
                 'question_content': 'Are there any affiliated (identical ownership or 100% Project-owned) company structures which will appear on any project Agreements, Permits or other documents?  If so, enter the company name here.  If not please enter “no”.?'},

                {'question_type': 'm', 'question_content': 'At what stage of development do you consider the project?',
                 'choices': [
                     'Early Stage (pre-execution of all or most Foundational Documents such as Site control, PPA, Interconnection, REC/Carbon Credit or other revenue, Feedstock)',
                     'Advanced Development (most foundational documents executed, critical permits issued)',
                     'Funding Term Sheet negotiated or executed',
                     'Post Term Sheet Due Diligence Period', 'Financial Close (for at least Construction Funding',
                     'Under Construction',
                     'COD/Final Financial Close']},

                {'question_type': 'b',
                 'question_content': 'Does the project have any ongoing, pending or potential legal actions as plaintiff or defendant?  Have any Demand letters written by Counsel been received?  (If so, indicate the name of the Counterparty or potential Counterparty.  If not enter “no”)'},

                {'question_type': 'b',
                 'question_content': 'Is the project sponsor the developer or owner of other energy projects (If so, how many, if not enter “no”)'},

                {'question_type': 'b',
                 'question_content': 'Is the project sponsor the developer or owner of other energy projects which use this type of generation and operations?   (If so, how many?  If not enter “no”)'}
            ]
        },

        {
            'section_name': 'Permitting',
            'text': 'Permitting is a key risk variable for any project. For each type of permit below, answer whether a permit is issued, submitted but not issued, application underway, application not started, or Not Required for this Project.    Below each question, a file upload prompt will follow.   If the Permit has been issued, submitted, or application completed, upload application.  If the Project does not require this type of Permit, or no Application has been started, please leave the file upload empty.',
            'questions': [
                {'question_type': 'm-f', 'question_content': 'What is the status of the Building Permit?',
                 'choices': ['Issued', 'Submitted but not Issued', 'Application Underway', 'Application not Started'],
                 'f_options': [0, 1, 2],
                 'file': 'Upload Building Permit or Application (main body without attachments).'},

                {'question_type': 'm-f', 'question_content': 'What is the status of the Air Permit?',
                 'choices': ['Issued', 'Submitted but not Issued', 'Application Underway', 'Application not Started'],
                 'f_options': [0, 1, 2],
                 'file': 'Upload Air Permit or Application (main body without attachments).'},

                {'question_type': 'm-f',
                 'question_content': 'What is the status of the Water treatment or related Permit?',
                 'choices': ['Issued', 'Submitted but not Issued', 'Application Underway', 'Application not Started'],
                 'f_options': [0, 1, 2],
                 'file': 'Upload Water Treatment Permit or other Application (main body without attachments).'},

                {'question_type': 'm-f', 'question_content': 'What is the status of the Building Permit?',
                 'choices': ['Issued', 'Submitted but not Issued', 'Application Underway', 'Application not Started'],
                 'f_options': [0, 1, 2],
                 'file': 'Upload Building Permit or Application (main body without attachments).'},

                {'question_type': 'm-f', 'question_content': 'What is the status of the Erosion Control Permit?',
                 'choices': ['Issued', 'Submitted but not Issued', 'Application Underway', 'Application not Started'],
                 'f_options': [0, 1, 2],
                 'file': 'Upload the Erosion Control Permit or Application'},

                {'question_type': 'm-f',
                 'question_content': 'What is the status of the Permanent Stormwater Plan/Permit?',
                 'choices': ['Issued', 'Submitted but not Issued', 'Application Underway', 'Application not Started'],
                 'f_options': [0, 1, 2],
                 'file': 'Upload Stormwater Permit/Plan or Application (main body without attachments).'},

                {'question_type': 'm-f',
                 'question_content': 'What is the status of the Land Disturbance or other similar?',
                 'choices': ['Issued', 'Submitted but not Issued', 'Application Underway', 'Application not Started'],
                 'f_options': [0, 1, 2],
                 'file': 'pload Land Disturbance or related Permit or Application (main body without attachments).'},

                {'question_type': 'm-f',
                 'question_content': 'What is the status of any other Permit not listed above (for example, boiler permit for a bioenergy project)?',
                 'choices': ['Issued', 'Submitted but not Issued', 'Application Underway', 'Application not Started'],
                 'f_options': [0, 1, 2],
                 'file': 'Upload other Permit (main body without attachments).'},
            ]
        },
        {
            'section_name': 'Design/Engineering',
            'text': 'For each type of Design/Engineering document listed below, answer whether it is complete and ready for construction with engineering stamp, in draft not stamped, not started, or Not Required for this Project. Below each question, a file upload prompt will follow.   If the design document has been issued, submitted, or application completed, upload application.  If the Project does not require this type of Permit, or no document has been started, please leave the file upload empty.',
            'questions': [
                {'question_type': 'm-f', 'question_content': 'Piping & Instrumentation Design (P & ID).',
                 'choices': ['Stamped, ready for Construction', 'In draft, not stamped', 'Not started',
                             'Not required for this project'], 'f_options': [0, 1],
                 'file': 'Upload document here.'},

                {'question_type': 'm-f', 'question_content': 'Construction Drawings.',
                 'choices': ['Stamped, ready for Construction', 'In draft, not stamped', 'Not started',
                             'Not required for this project'], 'f_options': [0, 1],
                 'file': 'Upload document here.'},

                {'question_type': 'm-f', 'question_content': 'Heat Balance (for bioenergy or other relevant project).',
                 'choices': ['Stamped, ready for Construction', 'In draft, not stamped', 'Not started',
                             'Not required for this project'], 'f_options': [0, 1],
                 'file': 'Upload document here.'},

                {'question_type': 'm-f',
                 'question_content': 'Solar Irradiance (for solar), Wind Resource (for Wind) or Feedstock (for bioenergy) Study complete?',
                 'choices': ['Issued with Certification or Stamp', 'In draft, not stamped', 'Not started',
                             'Not required for this project'], 'f_options': [0, 1],
                 'file': 'Upload document here.'},

                {'question_type': 'm-f',
                 'question_content': 'Independent Engineering Report with Design/Engineer section.',
                 'choices': ['Final, stamped', 'In draft, not stamped', 'Not started', 'Not required for this project'],
                 'f_options': [0, 1],
                 'file': 'Upload document here.'},

                {'question_type': 'm-f', 'question_content': 'Other Engineering Document.',
                 'choices': ['Stamped, ready for Construction', 'In draft, not stamped', 'Not started',
                             'Not required for this project'], 'f_options': [0, 1],
                 'file': 'Upload document here.'},
            ]
        },

        {
            'section_name': 'Project Agreements',
            'text': 'Project Agreements—the contracts with counterparties for revenue, interconnection, expenses and services, and more—are the key to understanding project risk.   The section below looks to capture these Agreements.  AI will be utilized to scan the documents for a variety of risk factors, such as non-commercially standard forms, counterparties with red flags in the public records, or contracts not in the proper project entity name.',
            'questions': [
                {'question_type': 'f', 'question_content': 'Primary Revenue Agreement'},

                {'question_type': 'i-f',
                 'question_content': 'Any other Sources of Revenue from energy sales listed in the Proforma',
                 'items': [], 'text': 'Please enter the name of the revenue source.'},

                {'question_type': 'f', 'question_content': 'Interconnection Agreement.'},

                {'question_type': 'i-f',
                 'question_content': 'Are there any other Agreements related to Interconnection, such as a System Impact Study referenced by the Interconnection Agreement?',
                 'items': [], 'text': 'Please enter the name of the file.'},

                {'question_type': 'f',
                 'question_content': 'Upload Agreements related to the sale of environmental commodities, like RECs or Carbon Credits.'},

                {'question_type': 'i-f',
                 'question_content': 'Are there any other sources of similar Agreements listed in the Proforma?',
                 'items': [], 'text': 'Please enter the source name of the file.'},

                {'question_type': 'f', 'question_content': 'Upload the EPC Agreement.'},

                {'question_type': 'i-f',
                 'question_content': 'Are there any other documents attached to EPC Agreement, such as exhibits or schedules?',
                 'items': [], 'text': 'Please enter the name of the file.'},

                {'question_type': 'f', 'question_content': 'Upload the Operations and Maintenance Agreement.'},

                {'question_type': 'i-f',
                 'question_content': 'Are there any other documents attached to O&M Agreement, such as exhibits or schedules?',
                 'items': [], 'text': 'Please enter the name of the file.'},

                {'question_type': 'f',
                 'question_content': 'Upload any Agreements related to other ongoing expenses involved in running the project (e.g., waste disposal).'},

                {'question_type': 'i-f',
                 'question_content': 'Are there any other Agreements related to project expenses?',
                 'items': [], 'text': 'Please enter the name of the file.'},

                {'question_type': 'f',
                 'question_content': 'Upload any Agreements related to the sale of any other project sidestreams (e.g., for a bioenergy project, this could be an ash, biochar, or digestate).'},

                {'question_type': 'i-f',
                 'question_content': 'Are there any other documents attached to this Agreement, such as exhibits or schedules?',
                 'items': [], 'text': 'Please enter the name of the file.'},

                {'question_type': 'i-f',
                 'question_content': 'Upload any other documents which are considered by the Sponsor to be essential project documents but which have not been requested before.',
                 'items': [], 'text': 'Please enter the name of the file.'},

                {'question_type': 'i-f',
                 'question_content': 'Are there any other relevant documents, Agreements, Reports, exhibits or schedules which should be considered in a risk analysis?',
                 'items': [], 'text': 'Please enter the name of the file.'},
            ]
        },
        {
            'section_name': 'Additional questions related to key project variables',
            'text': '',
            'questions': [
                {'question_type': 'b',
                 'question_content': 'Are any of the sources of revenue projected from market sales, or are they all contracted for the life of the project?'},
                {'question_type': 'b',
                 'question_content': 'For market revenue, what basis is used to project the volume of revenue?'},
                {'question_type': 'b',
                 'question_content': 'Do the revenue projections include escalation, or is the source of revenue set for the life of the project?'},
                {'question_type': 'b',
                 'question_content': 'Is there an Operations and Maintenance Contract in place (or planned) for the project?'},
                {'question_type': 'b',
                 'question_content': 'What is the inflation escalator for the operational expense? If zero, why?'},

                {'question_type': 'm-b',
                 'question_content': 'Does the project have any waste streams which require disposal on an ongoing basis?',
                 'choices': ['Yes', 'No'], 'f_options': [0],
                 'blank': 'What is the plan and cost for disposal?'},

                {'question_type': 'b', 'question_content': 'How many operating hours are projected for the project?'},
                {'question_type': 'b',
                 'question_content': 'How are the operating hours supported/verified (solar or wind studies, engineering reports, EPC guarantees?)'}
            ]
        },
        {
            'section_name': 'ESG (Environment, Social, Governance) factors',
            'text': 'For an increasing number of projects, ESG factors are very important.  This may be to ensure that the project is "best in class" for a variety of audiences, or (particularly in the case of funding which originates from European sources) it may be a critical factor in obtaining any funding. ESG factors can also be critical to make the project eligible for certain federal funding under multiple new initiatives such as the DOE’s Justice 40 program. \nThis section has a large number of questions related to ESG variables.   It is understood that some funding sources do not have ESG requirements, and the CLIENTscoreTM can be adjusted to reflect this by reducing the weight of ESG factors in the CEARTscore™. It is advised that the ESG section be completed regardless, because future funding sources or other requirements may apply to the project.   However, if a funder does not consider ESG requirements, and their  CLIENTscoreTM  may weight the ESG questions at zero and they may advice the Sponsor it is okay to skip the section. \nThe questions are to the Project, not the Sponsor, so the questions are directed at the entity operating the project, whether the Sponsor or an outside O&M group. The questions are progressively harder, starting with basic ESG questions, and progress to higher level European Union standard questions. If a question is confusing or unfamiliar, just answer no.',
            'questions': [
                {'question_type': 't', 'question_content': 'Standard ESG variables'},
                {'question_type': 'm', 'question_content': 'Does the project have a materials recycling program?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm',
                 'question_content': 'Does the project follow wage standards that meet or exceed the Davis-Bacon Act guidelines?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm', 'question_content': 'Does the project have an apprenticeship program?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm', 'question_content': 'Does the project have a materials recycling program?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm',
                 'question_content': 'Are vendors educated about ESG guidelines or given any specific ESG-related requirements?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm',
                 'question_content': 'Is there any monitoring of or reporting from vendors required in relation to ESG requirements?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm',
                 'question_content': 'Does the project implement energy efficiency measures, such as using renewable energy sources or optimizing energy use?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm',
                 'question_content': 'Is there a biodiversity impact assessment conducted, and are there measures in place to mitigate negative impacts on local ecosystems?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm',
                 'question_content': 'Are there programs in place for community engagement or contributions to local social initiatives?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm',
                 'question_content': 'Does the project have a system in place to monitor and report greenhouse gas emissions?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm',
                 'question_content': 'Are there measures to ensure ethical sourcing of materials, including rare earth elements, and how is this monitored?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm',
                 'question_content': 'Are ESG principles formalized in project documentation, such as an ESG manual or employee handbook?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm',
                 'question_content': 'Are there obligations written into the project finance documents that require certification, representations, or reporting to project funding partners regarding ESG compliance?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm',
                 'question_content': 'Does the project include a formal human rights policy, particularly in relation to labor practices and supply chain management?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm', 'question_content': 'Is there a diversity and inclusion policy in place?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm',
                 'question_content': 'Are there any certifications or audits required by third parties to validate ESG practices?',
                 'choices': ['Yes', 'No']},

                {'question_type': 't', 'question_content': 'European ESG Requirements'},
                {'question_type': 'm',
                 'question_content': 'Does the project align with the EU Taxonomy for sustainable activities?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm', 'question_content': 'Is compliance documented?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm',
                 'question_content': 'Are there specific measures in place to address the EU’s Corporate Sustainability Reporting Directive (CSRD) requirements?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm',
                 'question_content': 'How does the project ensure alignment with the EU’s Green Deal, particularly in terms of reducing carbon emissions?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm',
                 'question_content': 'Are there mechanisms for stakeholder engagement, including reporting to or involving local communities and NGOs?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm',
                 'question_content': 'How does the project address the EU’s focus on circular economy principles, such as waste reduction and resource efficiency?',
                 'choices': ['Yes', 'No']},

                {'question_type': 't', 'question_content': 'High level US ESG indictors'},
                {'question_type': 't', 'question_content': 'Environmental Indicators'},
                {'question_type': 'm',
                 'question_content': 'Does the project adhere to a sustainable land use policy, including site selection that prioritizes ecological conservation and the use of previously disturbed sites?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm',
                 'question_content': 'Is there a documented Net Zero Improvement Plan in place, including third-party assessments and measures for carbon capture and sequestration?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm',
                 'question_content': 'Does the project have a comprehensive Site Waste Management Plan, including partnerships with organizations (like GFL Environmental with CPP) to ensure sustainable waste practices?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm',
                 'question_content': 'Are there documented processes for the ethical sourcing of rare earth materials, including supplier evaluations and environmental impact assessments?',
                 'choices': ['Yes', 'No']},
                {'question_type': 't', 'question_content': 'Social Indicators'},
                {'question_type': 'm',
                 'question_content': 'Does the project include a framework for engaging with local communities and stakeholders to align with environmental and socio-economic goals?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm',
                 'question_content': 'Are there guidelines ensuring fair labor practices, safe working conditions, and respect for human rights within the project and its supply chain?',
                 'choices': ['Yes', 'No']},
                {'question_type': 't', 'question_content': 'Governance Indicators'},
                {'question_type': 'm',
                 'question_content': 'Is there a system in place for ensuring compliance with all relevant environmental and social regulations, including regular monitoring and reporting?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm',
                 'question_content': 'Does the project enforce a Supplier Code of Conduct that covers areas such as human rights, environmental responsibility, and anti-corruption measures?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm',
                 'question_content': 'Are there clear policies and procedures for whistleblowing and handling grievances, ensuring protection against retaliation and maintaining confidentiality?',
                 'choices': ['Yes', 'No']},
                {'question_type': 'm',
                 'question_content': 'Is there an anti-bribery and corruption policy that includes training, monitoring, and enforcement mechanisms to prevent improper payments and corruption?',
                 'choices': ['Yes', 'No']}
            ]
        },
        {
            'section_name': 'IRA Tax Credits',
            'text': 'The 2022 Inflation Reduction Act significantly changed the capital stack of most renewable energy and carbon removal projects. The section below pertains to the credits created by this law, with document requests to verify the progress toward making the credits bankable.',
            'questions': [
                {'question_type': 'm',
                 'question_content': 'Does the project claim to capture any credits by the 2022 IRA law? (If no, please skip this section)',
                 'choices': ['Yes', 'No']},
                {'question_type': 'f', 'question_content': 'Upload the project Cost Segregation Document'},
                {'question_type': 'f', 'question_content': 'Upload the project Appraisal'},
                {'question_type': 'f', 'question_content': 'Upload the tax credit sale Term Sheet'},
                {'question_type': 'f', 'question_content': 'Upload the tax credit Purchase Sales Agreement'},
                {'question_type': 'f', 'question_content': 'Upload proof of apprenticeship program'},
                {'question_type': 'f',
                 'question_content': 'Upload proof of proper minimum wage (Davis-Bacon) requirements'},
            ]
        },
        {
            'section_name': 'Capital Stack',
            'text': 'Risk is significantly reduced when portions of the capital stack—the required funding for the project made up of debt, equity, grants or other government support, and other forms of capital—is more complete.  The following questions are designed to understand which parts of this requirement are already in place.      For each, if there is an indication capital, a Term Sheet (or Notice of Award for grants) and an Agreement are requested.   If an Agreement exists, you may hit submit with the Term Sheet/NOA blank.   If there is not currently an Agreement, you may hit submit with that line blank.  If both are blank, CEARTscore™  will assume that this portion of the capital stack is not filled.   In other words, if no funding is currently in place for the project, simply hit Submit with a blank line for all of the items below. \nNote—-since government loan guarantees occur behind debt funding, it is assumed that indication of that will occur in the debt documents.',
            'questions': [
                {'question_type': 'f', 'question_content': 'Upload a term sheet for Debt Funding'},
                {'question_type': 'f', 'question_content': 'Upload an Agreement for Debt Funding'},
                {'question_type': 'f', 'question_content': 'Upload a term sheet for Equity Funding'},
                {'question_type': 'f', 'question_content': 'Upload an Agreement for Equity Funding'},
                {'question_type': 'f',
                 'question_content': 'Upload a Notice of Award or Letter of Award for government Grant Funding'}
            ]
        }
    ]
    return example


ALL_STAGES = ['EARLY DEVELOPMENT', 'ADVANCED DEVELOPMENT', 'FUNDING TERM SHEET', 'DUE DILIGENCE', 'FINANCE CLOSE',
              'CONSTRUCTION', 'COD']


def test_pdf(init_date='06/12/24', init_score='59', num_resub='2', client_stage=6, ceart_stage=4):
    dict_text = {
        'title': ['RED FLAG REPORT', 'ENERGY PROJECT RISK ASSESSMENT'],
        'name': 'Sample Project',
        'Summary': [
            'SompleProject ("SP") is a bioenergy project located in eastern North Carolina, utilizing ag waste biomass to make power and steam which is sold to major utilities and co-Iocoted industrial off-takers. The facility is set for a COD date of December 2024 and will utilize 260 tpd of fuel tocreote 26 MW of power.'],
        'DASHBOARD SNAPSHOT': [
            'The CEARTscore™  platform creates two scores from the somedata - a CEARTscore™ , which uses algorithms to weight various risk factors using industry-wide criteria. This score is comporobleocross all energy projects and provides a baseline for general investment risk. For this project, the score is:',
            'The following chart illustrates the scores which have the most weight inﬂuencing this CEARTscore™ ',
            'Additionally, each CEART Client has the capability to CLIENTSCORE change the weight of the variables to create a score that is ﬁtted to their investment thesis or framework. That is, one client may be focused on projects which have the least amount of exposure to contract and counter—porty credit risk. Another client may be focused on identifying projects with the best score for ESG factors. Each client has the ability to change the relative weights of these variables, to make each more or less important, so that their score ﬁts their precise needs. This is called the CLIENTSCORE. The CLIENTSCORE for this project is:',
            'The following chart illustrates the scores which have the most weight inﬂuencing this CLIENTSCORE',
            'The project originally submitted for a score on {}, and received a score of {}. It has updated more information on {} occasions, and received scores as indicated to the right'.format(
                init_date, init_score, num_resub),
            'The project sponsor has reported the development stage of the project as #{}, or the {} Phase:'.format(
                client_stage + 1, ALL_STAGES[client_stage]),
            'The CEART platform estimates the development stage to be #{}, or {}:'.format(ceart_stage + 1,
                                                                                          ALL_STAGES[ceart_stage]),
            'All of this information is available on the CEART dashboard. This report is designed to convey, along with that high level information, a more detailed indicator or which project variables have caused the score to be more positive or negative. The following sections give a more detailed description of how the scores above were determined. The purpose of the descriptions are to allow project sponsors to improve the risk factors which are likely to arise in project due diligence.',
            'The descriptions are also intended to assist the due diligence team in quickly focusing on areas in which risk factors likely reside—hopefully cutting the time required between project term sheet execution and due diligence.'],
        'GENERAL SCORE': [
            'The CEARTscore™  is generated through a project analysis which utilizes multiple categories of riskanalysis. Each section below speaks to one of those categories with context for how it has affected the score. For each category, variables which have negatively affected the score, and should be subject to further diligence, are indicated in red. Positive factors are green.'],
        'COMPLETENESS': {
            'general': 'The project was ranked above average, but not excellent, for general completeness.',
            'red items': [{'description': 'Some expected project documents were not found:',
                           'items': ['Interconnection Agreement',
                                     'Revenue agreements supporting all revenue projections',
                                     'Operation and Maintenance Contract']},
                          {'description': 'A memo was submitted rather than Independent Engineer report', 'items': []},
                          {'description': 'A building permit was submitted, but not any environmental reports',
                           'items': []},
                          {
                              'description': 'Project sponsor information was inadequate to do a complete stakeholder quality check.',
                              'items': ['The sponsor/developer credit information could not be obtained.']}
                          ],
            'green items': [
                {'description': 'Critical documents were found to support completeness of development level',
                 'items': []},
                {'description': 'Permits have largely been issued', 'items': []},
                ]
            },
        'PRO FORMA': {
            'general': 'An analysis of the SP pro forma generated a score of 78.\nThe reasons for this score:',
            'red items': [{'description': 'No provision for inﬂation cost increases', 'items': []},
                          {'description': 'Missing variables', 'items': ['dispensation of an ash waste stream']},
                          {'description': 'The pro forma does not seem to line up precisely contract terms:', 'items': [
                              'The EPC guaranteed performance is less than the revenue claimed',
                              'ROI is based on 15 year projections, contracts cover shorter duration.',
                              'Capex support from USDA grant indicated, no documentation submitted',
                              'Merchant carbon credit revenue is not bankable.'
                          ]},
                          ],
            'green items': [{'description': 'The pro forma is essentially complete.', 'items': []},
                            {'description': 'The pro forma covers the correct timeframes indicated in contracts',
                             'items': []},
                            {'description': 'The pro forma includes key variables such as', 'items': [
                                'cost of capital',
                                'energy production',
                                'production levels',
                                'revenue and expense information'
                            ]}
                            ],
            },
        'PROJECT LEGAL DOCUMENTS': {
            'general': 'The following factors were found in the onolysis of the project legal documents and integrated into the CEARTscore™ :',
            'Power Purchase': {'items': [
                'Five year pricing reodjustment Ieoves revenue downgrode risk',
                'Substontiol utility company liquidated domoge risk for non-performonce',
                'Signiﬁcant revenue risk from simple utility terminotion clauses'
            ], 'colors': ['red', 'red', 'red']},
            'Other Revenue': {'items': [
                'No long term corbon credit controct to match booked revenue',
                'Side streom woste soles not enforceoble ("take or pay"), odding revenue risk'
            ], 'colors': ['red', 'red']},
            'Site Control': {'items': [
                'Renewoble of lease before end-of project creotes site control risk',
                'Project Ieose indicotes ingress/egress risk'
            ], 'colors': ['red', 'red']},
            'Feedstock': {'items': [
                'Feedstock agreement hos no enforceoble domoges'
            ], 'colors': ['red']},
            'Interconnection': {'items': [
                'No notice provisions for closing Interconnection for maintenance',
                'No grid Impact Study submitted, but referred to in the IC agreement',
                'IC deodline Ieoves inodequote buffer for project deloys'
            ], 'colors': ['red', 'red', 'red']},
            'Operations and Mantainess': {'items': [
                'O&M controct hos no performance penalties or guorontees.',
                'Minimolly deﬁned scope Ieoves bock—chorge/Chonge order risk'
            ], 'colors': ['red', 'red']},
            'Counterparty Risk': {'items': [
                'No negotive media or public record (regulotory/Iowsuit) indication found',
                'No credit record information available, recommend review'
            ], 'colors': ['red', 'red']}
            },
        'DESIGN DOCUMENT': {'general': '',
                            'red items': [
                                {'description': 'Design documents ore not ready for construction, presenting cost risk',
                                 'items': []},
                                {'description': 'The design concept validation by IE mitigotes risk', 'items': []}
                                ],
                            'green items': [
                                {'description': 'Risk mitigated by moss/heot balance and P&ID with engineer stomp',
                                 'items': []},
                                ]
                            },
        'SITE VARIABLES': {'general': '',
                           'Surrounding Community': {'items': [
                               'Positive media coverage found, no negative mentions',
                               'No media references to opposition to energy projects',
                               'Porcel listed as Disodvontoged ond/or Rurol, check diligence env. Justice'
                           ], 'colors': ['green', 'green', 'red']},
                           'Access': {'items': [
                               'Ingress/Egress issues'
                           ], 'colors': ['red']},
                           'Weather Hozords': {'items': [
                               'The site is in o 500-yeor ﬂood plon',
                               'Region prone to high wind events, should be addressed in design docs'
                           ], 'colors': ['red', 'red']},
                           'BrownﬁeId/Pollution': {'items': [
                               'Clean Phase | Environmental Report'
                           ], 'colors': ['green']},
                           'Interconnection': {'items': [
                               'No notice provisions for closing Interconnection for maintenance',
                               'No grid Impact Study submitted, but referred to in the IC agreement',
                               'IC deodline Ieoves inodequote buffer for project deloys'
                           ], 'colors': ['red', 'red', 'red']},
                           'Operations and Mantainess': {'items': [
                               'O&M controct hos no performance penalties or guorontees.',
                               'Minimolly deﬁned scope Ieoves bock—chorge/Chonge order risk'
                           ], 'colors': ['red', 'red']},
                           'Counterparty Risk': {'items': [
                               'No negotive media or public record (regulotory/Iowsuit) indication found',
                               'No credit record information available, recommend review'
                           ], 'colors': ['red', 'red']}
                           },
        'ENGINEERING, PERMITTING AND CONSTRUCTION (EPC) DOCUMENTS': {'general': '',
                                                                     'red items': [{
                                                                                       'description': 'EPC agreement not executed/droft form',
                                                                                       'items': []},
                                                                                   {
                                                                                       'description': 'The EPC counterporty and ﬁnanciols unknown, performance risk',
                                                                                       'items': []},
                                                                                   ],
                                                                     'green items': [{
                                                                                         'description': 'The EPC pricing and dates match the proforma, contributes positively to the score.',
                                                                                         'items': []}
                                                                                     ]
                                                                     },
        'TECHNOLOGY VARIABLES': {'general': '',
                                 'red items': [],
                                 'green items': [{
                                                     'description': 'The core contemplated technology is fully commercialized—with reference projects provided, which contributes positively to the score.',
                                                     'items': []}]
                                 },
        'SPONSOR AND TEAM VARIABLES': {'general': '',
                                       'black items': [{'description': 'test neutral item 1', 'items': []},
                                                       {'description': 'test neutral item 2',
                                                        'items': ['sub1', 'sub2']}],
                                       'red items': [],
                                       'green items': [{
                                                           'description': 'Background check of the sponsor and key project leaders relatively positive. One member did have some nonviolent offenses which might be clariﬁed by stakeholders.',
                                                           'items': []},
                                                       {
                                                           'description': 'The project team is suitably experienced with the type of project and technology. Adequate professional credentials are present.',
                                                           'items': []}
                                                       ]
                                       },
        'DEVELOPER RECORD AND BEST PRACTICES': {'general': '',
                                                'red items': [
                                                    {
                                                        'description': 'Sponsor does not have other similar projects in existence.',
                                                        'items': []}
                                                ],
                                                'green items': [
                                                    {
                                                        'description': 'Sponsor personnel have leadership with similar projects.',
                                                        'items': []}
                                                ]
                                                },
        'PERMITTING': {'general': '',
                       'red items': [
                           {'description': 'Construction permits have not been issued.', 'items': []}
                       ],
                       'green items': [{'description': 'Critical permits have been issued and appear to be current.',
                                        'items': [
                                            'Air permit issued xx/xx/xx',
                                            'Stormwater permit issued xx/xx/xx',
                                            'Wastewater permit issued xx/xx/xx'
                                        ]}
                                       ]
                       },
        'INDEPENDENT ENGINEER OR EQUIVALENT REPORT': {'general': '',
                                                      'red items': [],
                                                      'green items': [{
                                                                          'description': 'A signed IE report has been issued, with current/volid PE stamp',
                                                                          'items': []},
                                                                      {
                                                                          'description': 'The report acknowledges several of the risk factors listed here and improves the score.',
                                                                          'items': []}
                                                                      ]
                                                      },
        'CAPITAL STACK': {'general': '',
                          'red items': [{'description': 'No fully executed Agreement for debt', 'items': []},
                                        {'description': 'No private equity instrument was located', 'items': []},
                                        {
                                            'description': 'No documentation of method of valuation or sale for ITC was detected',
                                            'items': []},
                                        ],
                          'green items': [
                              {'description': 'A term sheet for project debt has been executed', 'items': []},
                              {'description': 'Basic threshold eligibility for Investment Tax Credits', 'items': []},
                              {
                                  'description': 'Basic threshold eligibility for New Market Tax Credit and USDA-eligible rural tract',
                                  'items': []},
                              {'description': 'Basic threshold eligibility for USDA REAP grant', 'items': []}
                              ]
                          },

    }
    return dict_text


if __name__ == '__main__':
    print(example_score(1, 1)[0])

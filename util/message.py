# -*- coding: utf-8 -*-
"""
File Name：     messae
Description :
date：          2024/2/22 022
"""
import httpx
from settings import X_API_KEY, PROXY

headers = {
    "accept": "application/json",
    "X-API-KEY": X_API_KEY
}
send_massage_url = "https://api-beta.copilot.com/v1/messages"


def create_message_channels(member_ship_entity_id):
    """
    创建消息通道
    :param member_ship_entity_id:
    :return:
    """
    url = "https://api.copilot.com/v1/message-channels"

    payload = {"membershipType": "individual", "membershipEntityId": member_ship_entity_id}
    with httpx.Client(proxies=PROXY) as client:
        resp = client.post(url, json=payload, headers=headers)
        if resp.status_code == 200:
            return True
        else:
            return False


def send_message(member_ship_entity_id: str = "43443c7b-0141-40c0-9652-8f5467ab2cf5", text: str = "py测试消息",
                 senderId: str = None):
    """
    copilot发送消息
    :param senderId: 发送人id
    :param member_ship_entity_id:用户id
    :param text:发送文本
    """
    with httpx.Client(proxies=PROXY) as client:
        channels_url = f"https://api-beta.copilot.com/v1/message-channels?membershipEntityId={member_ship_entity_id}&limit=100"
        resp = client.get(channels_url, headers=headers)
        if resp.json().get('data'):
            channelId = resp.json().get('data')[0].get('id')
            client.post(send_massage_url, headers=headers, json={
                "channelId": channelId,
                "text": text,
                "senderId": senderId
            })
            print({"message": "问卷url已发送至用户"})
        else:
            print({"message": "channelID不存在，开始创建消息通道"})
            create_status = create_message_channels(member_ship_entity_id)
            if create_status:
                send_message(member_ship_entity_id, text)
            else:
                print({"message": "消息发送失败"})


def search_user(user_id: str):
    url = f"https://api.copilot.com/v1/clients/{user_id}"
    with httpx.Client(proxies=PROXY) as client:
        resp = client.get(url, headers=headers)
        if resp.status_code == 200:
            return resp.json()
        else:
            return False


def search_company(companyId: str):
    url = f"https://api.copilot.com/v1/companies/{companyId}"
    with httpx.Client(proxies=PROXY) as client:
        resp = client.get(url, headers=headers)
        if resp.status_code == 200:
            return resp.json()
        else:
            return False


def check_invoices(invoice_id: str):
    url = f"https://api.copilot.com/v1/invoices/{invoice_id}"
    with httpx.Client() as client:
        resp = client.get(url, headers=headers)
        if resp.status_code == 400:
            return False, resp.json()
        else:
            return resp.json().get('status'), resp.json()


if __name__ == '__main__':
    send_message()

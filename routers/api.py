# -*- coding: utf-8 -*-
"""
File Name：     api
Description :
date：          2024/2/22 022
"""
import json
import os
import urllib.parse

import httpx
import numpy as np
from hashlib import md5
from typing import Union

import pendulum
from fastapi.responses import JSONResponse
from util.calc import calculate_weighted_score
from util.parse import parse_text_to_form, heart_texts, custom_json_encoder, heart_forms, heart_datas, ObjectId, \
    parse_form_to_datas, heart_weight, first_parse_form_to_datas, heart_first_forms, get_users, heart_orders, \
    send_message, sub_calc, heart_save
from fastapi import APIRouter, BackgroundTasks, File, UploadFile, Body, Path, Request, HTTPException
from util.calc import example_score, test_ret, test_pdf
from util.message import check_invoices

UPLOAD_DIR = "static/"

router = APIRouter(prefix='/api')


@router.post("/upload/")
async def upload_file(file: UploadFile = File()):
    file_contents = await file.read()
    file_md5 = md5(file_contents).hexdigest()
    file_ext = os.path.splitext(file.filename)[-1]
    new_filename = file_md5 + file_ext
    os.makedirs(UPLOAD_DIR, exist_ok=True)
    with open(os.path.join(UPLOAD_DIR, new_filename), "wb") as f:
        f.write(file_contents)
    return {"filename": f"/static/{new_filename}"}


@router.post("/text-to-form/")
async def text_to_form(background_tasks: BackgroundTasks,
                       text: str = Body(),
                       user_id: str = Body(),
                       internal_id: Union[None, str] = Body(None)):
    """
    解析用户上传的文本，状态码202，后台解析
    :param internal_id: 内部id
    :param user_id:用户id
    :param text: 用户上传的文本
    :param background_tasks: 解析函数
    :return:
    """
    heart_first_forms_source = heart_first_forms.find_one({"_id": ObjectId(internal_id)},
                                                          {'forms': 1, 'project_type': 1})
    project_type = heart_first_forms_source['project_type']

    heart_first_forms_source = heart_first_forms_source.get(
        'forms') if heart_first_forms_source and heart_first_forms_source.get(
        'forms') else []

    heart_texts_source = heart_texts.find_one({"_id": ObjectId(internal_id)}, {'text': 1})
    if heart_texts_source:
        heart_texts_source = heart_texts_source.get('text') if heart_texts_source.get('text') else []

    if heart_texts_source and heart_first_forms_source:
        if len(heart_first_forms_source) < len(heart_texts_source):
            raise HTTPException(status_code=400, detail="Please proceed and complete the form submission.")
    background_tasks.add_task(parse_text_to_form, text, user_id, internal_id)

    # heart_first_forms_source = heart_first_forms.find_one({"_id": ObjectId(internal_id)}, {'forms': 1})
    # heart_first_forms_source = heart_first_forms_source.get(
    #     'forms') if heart_first_forms_source and heart_first_forms_source.get(
    #     'forms') else []
    #
    # heart_texts_source = heart_texts.find_one({"_id": ObjectId(internal_id)}, {'text': 1})
    # heart_texts_source = heart_texts_source.get('text') if heart_texts_source.get('text') else []

    # if heart_texts_source and heart_first_forms_source:
    #     if len(heart_first_forms_source) == len(heart_texts_source):
    #         return JSONResponse(
    #             status_code=202,
    #             content={"message": "We have received your submission. You will receive our survey shortly."})
    #     else:
    #         return JSONResponse(
    #             status_code=202,
    #             content={"message": "Please upload your proforma before submission"})


@router.get("/first_from_data")
async def first_from_data():
    return test_ret()


@router.post("/first-form-to-data/")
async def first_form_to_data(background_tasks: BackgroundTasks,
                             request: Request,
                             form: list = Body(),
                             user_id: str = Body(),
                             internal_id: str = Body()):
    heart_first_forms_source = heart_first_forms.find_one({"_id": ObjectId(internal_id)},
                                                          {'forms': 1, 'project_type': 1})
    project_type = heart_first_forms_source['project_type']

    # if 'last_time' in heart_datas.find_one({"_id": ObjectId(internal_id)}, {}):
    #     if not sub_calc(project_type, user_id, None):
    #         raise HTTPException(status_code=400, detail="No subscriptions available")

    if heart_first_forms_source:
        heart_first_forms_source = heart_first_forms_source.get('forms') if heart_first_forms_source.get(
            'forms') else []

    heart_texts_source = heart_texts.find_one({"_id": ObjectId(internal_id)}, {'text': 1})
    if heart_texts_source:
        heart_texts_source = heart_texts_source.get('text') if heart_texts_source.get('text') else []

    if heart_texts_source and heart_first_forms_source:
        if len(heart_first_forms_source) > len(heart_texts_source):
            raise HTTPException(status_code=400, detail="Please proceed to complete the document submission form.")
    inside = True if 'project' in request.headers['host'] else False
    background_tasks.add_task(first_parse_form_to_datas, form, user_id, internal_id, inside)

    # heart_first_forms_source = heart_first_forms.find_one({"_id": ObjectId(internal_id)}, {'forms': 1})
    # heart_first_forms_source = heart_first_forms_source.get(
    #     'forms') if heart_first_forms_source and heart_first_forms_source.get(
    #     'forms') else []
    #
    # heart_texts_source = heart_texts.find_one({"_id": ObjectId(internal_id)}, {'text': 1})
    # heart_texts_source = heart_texts_source.get('text') if heart_texts_source.get('text') else []
    # if heart_texts_source and heart_first_forms_source:
    #     if len(heart_first_forms_source) == len(heart_texts_source):
    #         return JSONResponse(
    #             status_code=202,
    #             content={"message": "We have received your submission. Your score dashboard will be available soon."})
    #     else:
    #         return JSONResponse(
    #             status_code=202,
    #             content={"message": "Please upload your file before submission"})


@router.post("/form-to-data/")
async def form_to_data(background_tasks: BackgroundTasks,
                       request: Request,
                       form: list = Body(),
                       user_id: str = Body(),
                       internal_id: str = Body()):
    """
    解析用户修改后的表单，状态码202，后台解析
    :param request: 。。。
    :param internal_id: 内部表单id
    :param user_id:用户id
    :param form: 用户修改后的表单
    :param background_tasks: 解析函数
    :return:
    """
    inside = True if 'project' in request.headers['host'] else False
    background_tasks.add_task(parse_form_to_datas, form, user_id, internal_id, inside)
    return JSONResponse(
        status_code=202,
        content={"message": "We have received your submission. Your score dashboard will be available soon."})


@router.post("/calc_weight_score")
async def forms(request: Request, datas: dict = Body(
    {
        'score': {
            'site and interconnection': 89,
            'revenue': 74,
            'sponsor and counterparty': 73,
            'proforma quality': 71,
            'construction risk': 68,
            'performance risk': 59,
            'esg': 48,
            'doc set quality': 41,
            'contract bakability': 38,
            'capital stack': 29
        },
        'user_score': {
            'site and interconnection': 50,
            'revenue': 100,
            'sponsor and counterparty': 50,
            'proforma quality': 50,
            'construction risk': 50,
            'performance risk': 100,
            'esg': 50,
            'doc set quality': 50,
            'contract bakability': 50,
            'capital stack': 50
        }
    }
),
                internal_id: str = Body(),
                custom: bool = Body(False),
                history: bool = Body(False)
                ):
    """
    计算用户权重得分
    :param request:
    :param history: 是否更新历史分数
    :param custom: 是否用户自定义权重默认False
    :param datas: 默认权重
    :param user_id: 用户id
    :param internal_id: 问卷id
    :return:
    """
    weight = dict(sorted(datas['score'].items()))
    user_score = dict(sorted(datas['user_score'].items()))
    score = np.array(list(user_score.values()))
    weighted_score, client_stage, ceart_stage = calculate_weighted_score(weight)
    user_weighted_score, client_stage, ceart_stage = calculate_weighted_score(weight, score)
    inside = True if 'project' in request.headers['host'] else False
    if inside:
        """用户侧仅更新用户分数权重"""
        heart_datas.update_one({"_id": ObjectId(internal_id)},
                               {"$set": {
                                   "datas.user_score": datas['user_score'],
                                   "datas.user_weighted_score": user_weighted_score,
                                   "custom": custom
                               }}, upsert=True)
    else:
        """管理员侧更新用户分数权重和最近的分数"""
        heart_datas.update_one({"_id": ObjectId(internal_id)},
                               {"$set": {
                                   "datas.scores": datas['score'],
                                   "datas.weighted_score": weighted_score,
                                   "datas.user_score": datas['user_score'],
                                   "datas.user_weighted_score": user_weighted_score,
                                   "custom": custom
                               }}, upsert=True)
    if history and not inside:
        heart_datas_search = heart_datas.find_one({'_id': ObjectId(internal_id)})
        history_score = heart_datas_search['datas']['history_score'] if heart_datas_search else [
            {pendulum.now().to_date_string(): weighted_score}]
        history_score = history_score[:-1]
        history_score += [{pendulum.now().to_date_string(): weighted_score}] if heart_datas_search else []
        heart_datas.update_one({'_id': ObjectId(internal_id)},
                               {'$set': {"datas.history_score": []}}, upsert=True)
        heart_datas.update_one({'_id': ObjectId(internal_id)},
                               {'$set': {"datas.history_score": history_score}}, upsert=True)
    return {"user_weighted_score": user_weighted_score, "client_stage": client_stage, "ceart_stage": ceart_stage}


@router.delete("/forms/{form_id}")
async def delete_forms(form_id: str = Path(min_length=24, max_length=24)):
    """
    删除相关问卷流程
    :param form_id: 问卷id
    :return:
    """
    heart_texts.delete_many({"_id": ObjectId(form_id)})
    heart_forms.delete_many({"_id": ObjectId(form_id)})
    heart_datas.delete_many({"_id": ObjectId(form_id)})

    heart_first_forms.delete_many({"_id": ObjectId(form_id)})

    return {"message": "Operation completed."}


@router.get("/create/{user_id}/{project_name}/{project_type}")
async def create_project(user_id: str, project_name: str, project_type: str):
    if project_type in ['basic', 'professional', 'advanced']:
        project_id = ObjectId()
        heart_texts.insert_one(
            {"_id": project_id, "user_id": user_id, 'project_name': project_name, 'project_type': project_type})
        heart_forms.insert_one(
            {"_id": project_id, "user_id": user_id, 'project_name': project_name, 'project_type': project_type})
        heart_datas.insert_one(
            {"_id": project_id, "user_id": user_id, 'project_name': project_name, 'project_type': project_type})

        heart_first_forms.insert_one(
            {"_id": project_id, "user_id": user_id, 'project_name': project_name, 'project_type': project_type})

        return {"message": "Create completed."}
    else:
        raise HTTPException(500, "项目类型不合法")


@router.post("/save_data/{form_type}/{internal_id}")
async def save_data(internal_id: str, form_type: str, datas: Union[list, dict] = Body()):
    """
    暂存表单数据
    :param form_type: texts/forms/datas/first_forms
    :param datas: 表单数据
    :return:
    """
    if form_type == "texts":
        ...
        # heart_texts.update_one({'_id': ObjectId(internal_id)},
        #                        {'$set': {"text": datas}}, upsert=True)
    elif form_type == "forms":
        ...
        # heart_forms.update_one({'_id': ObjectId(internal_id)},
        #                        {'$set': {"forms": datas}}, upsert=True)
    elif form_type == "datas":
        ...
        # heart_datas.update_one({'_id': ObjectId(internal_id)},
        #                        {'$set': {"datas": datas}}, upsert=True)
    elif form_type == "first_forms":
        # heart_first_forms.update_one({'_id': ObjectId(internal_id)},
        #                              {'$set': {"forms": datas}}, upsert=True)
        heart_save.update_one({'_id': ObjectId(internal_id)},
                              {'$set': {"forms": datas}}, upsert=True)
    else:
        raise HTTPException(500, "参数不合法")
    return {"msg": "success"}


@router.get("/save_data/{form_type}/{internal_id}")
async def save_data(internal_id: str, form_type: str):
    """
    获取暂存数据
    :param internal_id:
    :param form_type:
    :return:
    """
    first_forms_data = heart_save.find_one({"_id": ObjectId(internal_id)}, {'forms': 1})
    if first_forms_data:
        return first_forms_data.get('forms')
    else:
        return None


@router.get("/search/{user_id}")
async def search(user_id: str, skip: int = 0, limit: int = 10):
    """
    搜索用户问卷流程及信息
    :param user_id:
    :param skip:
    :param limit:
    :return:
    """
    user_docs = heart_texts.aggregate([
        {"$match": {"user_id": user_id}},
        {"$lookup": {"from": "texts", "localField": "_id", "foreignField": "_id", "as": "texts"}},
        {"$lookup": {"from": "forms", "localField": "_id", "foreignField": "_id", "as": "forms"}},
        {"$lookup": {"from": "datas", "localField": "_id", "foreignField": "_id", "as": "datas"}},
        {"$lookup": {"from": "first_forms", "localField": "_id", "foreignField": "_id", "as": "first_forms"}},
        {"$project": {"texts": 1, "forms": 1, "datas": 1, "first_forms": 1}},
        {"$skip": skip},
        {"$limit": limit}
    ])
    user_docs = custom_json_encoder(user_docs)
    result = dict(data=user_docs, total=heart_texts.count_documents({'user_id': user_id}))
    return result


@router.put("/project_name/{form_id}/{project_name}")
async def update_project_name(form_id: str = Path(min_length=24, max_length=24), project_name: str = Path()):
    heart_texts.update_one(
        {'_id': ObjectId(form_id)}, {'$set': {'project_name': project_name}}, upsert=True)
    return JSONResponse(
        content={"message": "Project name updated successfully."})


@router.put("/apply_all")
async def app_all(user_id: str,
                  weight: dict = Body({
                      'site and interconnection': 50,
                      'revenue': 100,
                      'sponsor and counterparty': 50,
                      'proforma quality': 50,
                      'construction risk': 50,
                      'performance risk': 100,
                      'esg': 50,
                      'doc set quality': 50,
                      'contract bankability': 50,
                      'capital stack': 50
                  })):
    if len(weight) == len(example_score(1, 1)[0]):
        heart_weight.delete_many({"user_id": user_id})
        heart_weight.update_one({"user_id": user_id}, {"$set": weight}, upsert=True)
        return JSONResponse(
            content={"message": "Successful application of weight."})
    else:
        return JSONResponse(
            content={"message": "Please refresh this page first."})


@router.get("/get_weight/{user_id}")
async def get_weight(user_id: str):
    return JSONResponse(content=heart_weight.find_one({'user_id': user_id}, {'_id': 0, 'user_id': 0}))


@router.get("/project/{project_id}")
async def get_project(project_id: str):
    project_docs = heart_texts.aggregate([
        {"$match": {"_id": ObjectId(project_id)}},
        {"$lookup": {"from": "texts", "localField": "_id", "foreignField": "_id", "as": "texts"}},
        {"$lookup": {"from": "forms", "localField": "_id", "foreignField": "_id", "as": "forms"}},
        {"$lookup": {"from": "datas", "localField": "_id", "foreignField": "_id", "as": "datas"}},
        {"$lookup": {"from": "first_forms", "localField": "_id", "foreignField": "_id", "as": "first_forms"}},
        {"$project": {"texts": 1, "forms": 1, "datas": 1, "first_forms": 1}},
    ])
    user_docs = custom_json_encoder(project_docs)
    result = dict(data=user_docs, total=heart_texts.count_documents({"_id": ObjectId(project_id)}))
    return result


@router.put('/project/{project_id}/{project_type}')
async def update_project_type(project_id: str, project_type: str):
    """
    更新项目类型
    :param project_id:
    :param project_type:
    :return:
    """
    heart_first_forms_source = heart_first_forms.find_one({"_id": ObjectId(project_id)},
                                                          {'project_type': 1, 'user_id': 1})
    project_types = ['basic', 'professional', 'advanced']
    if project_types.index(project_type) <= project_types.index(heart_first_forms_source['project_type']):
        return {"message": "Downgrade operation is not allowed"}
    else:
        try:
            if sub_calc(project_type, heart_first_forms_source['user_id'], heart_first_forms_source['project_type']):
                project_id = ObjectId(project_id)
                heart_texts.update_one({"_id": project_id}, {'$set': {'project_type': project_type}})
                heart_forms.update_one({"_id": project_id}, {'$set': {'project_type': project_type}})
                heart_datas.update_one({"_id": project_id}, {'$set': {'project_type': project_type}})

                heart_first_forms.update_one({"_id": project_id}, {'$set': {'project_type': project_type}})

                return {"message": "OK"}
            else:
                raise HTTPException(400, "No subscriptions available")
        except Exception as e:
            return {"message": e}


@router.get("/pdf/{project_id}")
async def get_pdf(project_id: str):
    """
    pdf信息
    :param project_id: 通过项目id查询项目历史数据。。。生成pdf数据
    :return: pdf信息
    """
    return test_pdf()


@router.get("/user/{user_id}")
async def get_user(user_id):
    """
    获取用户详细信息
    :param user_id: 用户id
    :return:
    """
    return get_users(user_id)


@router.post("/orders/create")
async def orders_create(basic: int = Body(), professional: int = Body(), advanced: int = Body(), user_id: str = Body()):
    """
    创建订单
    :param basic:
    :param professional:
    :param advanced:
    :param user_id:
    :return:
    """
    orders_create_db = heart_orders.insert_one(dict(
        basic=basic,
        professional=professional,
        advanced=advanced,
        use_basic=basic,
        use_professional=professional,
        use_advanced=advanced,
        user_id=user_id,
        create_time=pendulum.now(),
        invoice_url=None,
        type='general',
        status=None))
    # send_data = f"Payment request created: {orders_create_db.inserted_id}"
    send_data = f"I would like to subscribe for basic({basic})/professional({professional})/advanced({advanced}) project, orders id is {orders_create_db.inserted_id} "
    send_message(user_id, send_data, user_id)
    return JSONResponse(content=f"Payment request created: {orders_create_db.inserted_id}")


@router.post("/upgrade_order/create")
async def upgrade_order_create(user_id: str = Body(), project_id: str = Body()):
    """
    创建一次性升级订单，绑定项目id
    :param project_id:
    :param user_id:
    :return:
    """
    orders_create_db = heart_orders.insert_one(dict(
        user_id=user_id,
        project_id=project_id,
        create_time=pendulum.now(),
        invoice_url=None,
        status=None,
        use_status=False,
        type='dedicated'
    ))
    send_data = f"I want to upgrade the project ({project_id}) and increase the number of submissions, the order id is {orders_create_db.inserted_id}"
    send_message(user_id, send_data, user_id)
    return JSONResponse(content=f"Payment request created: {orders_create_db.inserted_id}")


@router.get("/search_dedicated/{project_id}")
async def search_dedicated_project_id(project_id: str):
    orders_many = heart_orders.find({'type': 'dedicated'})
    for order in orders_many:
        if order['invoice_url'] and order['status'] != 'paid':
            invoice_id = order['invoice_url'].split('/')[-1]
            check_in, order_info = check_invoices(invoice_id)
            if check_in:
                heart_orders.update_one({"_id": ObjectId(order["_id"])}, {"$set": {"status": check_in}})
                order['status'] = check_in
                order['info'] = order_info
    dedicated_order = heart_orders.find_one({"project_id": project_id,
                                             'type': 'dedicated',
                                             'use_status': False,
                                             'status': 'paid'})
    if dedicated_order:
        heart_orders.update_one({'_id': dedicated_order['_id']}, {"$set": {"use_status": True}})
        return True
    else:
        return False


@router.delete("/orders/{orders_id}")
async def orders_delete(orders_id: str):
    """
    删除订单
    :param orders_id:
    :return:
    """
    if heart_orders.find_one({"_id": ObjectId(orders_id)})['status'] == 'paid':
        raise HTTPException(400, detail="Paid orders cannot be deleted")
    else:
        heart_orders.delete_one({"_id": ObjectId(orders_id)})
        return JSONResponse(content="ok")


@router.get("/orders/get/{orders_id}")
async def orders_get(orders_id: str):
    """
    获取订单
    :param orders_id:
    :return:
    """
    order = heart_orders.find_one({"orders_id": orders_id})
    if order:
        order["_id"] = str(order["_id"])
        return order
    else:
        return None


@router.get("/orders/{user_id}")
async def get_orders(user_id: str):
    """
    获取订单列表
    :param user_id:
    :return:
    """
    orders_many = heart_orders.find({"user_id": user_id})
    res = []
    for order in orders_many:
        order["_id"] = str(order["_id"])
        if order['invoice_url']:
            invoice_id = order['invoice_url'].split('/')[-1]
            check_in, order_info = check_invoices(invoice_id)
            if check_in:
                heart_orders.update_one({"_id": ObjectId(order["_id"])}, {"$set": {"status": check_in}})
            order['status'] = check_in
            order['info'] = order_info
        res.append(order)
    return res


@router.post("/orders/invoice/{orders_id}")
async def orders_invoice(orders_id: str, invoice_url: str):
    """
    填充票据url
    :param orders_id:
    :param invoice_url:
    :return:
    """
    invoice_url = urllib.parse.unquote(invoice_url)
    orders_id = ObjectId(orders_id)
    if not heart_orders.find_one({"invoice_url": invoice_url}):
        if heart_orders.find_one({"_id": orders_id})['invoice_url']:
            raise HTTPException(400, detail="Payment link uploaded")
        else:
            invoice_id = invoice_url.split('/')[-1]
            check_in, orders_info = check_invoices(invoice_id)
            if check_in:
                heart_orders.update_one({"_id": orders_id}, {"$set": {"invoice_url": invoice_url, "status": check_in}})
                return JSONResponse(content=f"Payment link uploaded")
            else:
                raise HTTPException(400, detail="The payment link is incorrect. Please confirm it again.")
    else:
        raise HTTPException(400, detail="This invoice has been used")

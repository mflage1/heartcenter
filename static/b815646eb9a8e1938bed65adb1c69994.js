(function () {
    if(window.location.host.indexOf("168wangxiao") == -1){
        window.alert("此网站暂不支持该脚本，请查找相关168网校！")
        return
    }
    let body = document.body;
    // let classTitle = (document.querySelectorAll(".el-breadcrumb__inner")[1]).innerText
    // if(classTitle == localStorage.getItem("classTitle")){
    //     localStorage.setItem("classTitle",classTitle)
    // }else{
    //     localStorage.setItem("classTitle",classTitle)
    //     localStorage.setItem("index",0)
    // }
    let newDivAdd = document.createElement('div');
    newDivAdd.classList.add("newDivAdd")
    newDivAdd.style.width = "250px"
    newDivAdd.style.height = "350px"
    newDivAdd.style.background = "#fff"
    newDivAdd.style.position = "fixed"
    newDivAdd.style.top = "50%"
    newDivAdd.style.left = "150px"
    newDivAdd.style.transform = "translate(-50%, 0)"
    newDivAdd.style.zIndex = "999999999999999999999"
    newDivAdd.style.boxShadow = "0 15px 30px rgba(0,0,0,.3)"
    let contentDiv = document.createElement('div')
    contentDiv.classList.add("studyPList")
    contentDiv.style.width = "100%"
    contentDiv.style.height = "290px"
    contentDiv.style.overflow = "auto"
    let titleH3 = '<h3 class="myTitleH3" style="text-align: center;border-bottom: 1px solid #f0f0f7;height: 30px;">长大脚本</h3>'
    newDivAdd.innerHTML = titleH3
    let nodeList = document.querySelectorAll('.el-tree-node__content');
    let useNodeList = []
    let noUseCount = 0
    let index = -1
    // let localStoreIndex = localStorage.getItem('index')
    // if (localStoreIndex) {
    //     index = Number(localStoreIndex) - 1
    // } else {
    //     index = -1
    //     localStorage.setItem('index', -1)
    // }
    document.body.appendChild(newDivAdd)
    let myTitleH3 = document.querySelector('.myTitleH3')
    let newDivAddDrag = document.querySelector('.newDivAdd')
    myTitleH3.style.cursor = "move"
    myTitleH3.onmousedown = function (e) {
        e = e || window.event;
        let diffX = e.clientX - newDivAddDrag.offsetLeft;
        let diffY = e.clientY - newDivAddDrag.offsetTop;
        document.onmousemove = function (e) {
            e = e || window.event;
            let left = e.clientX - diffX;
            let top = e.clientY - diffY;
            // 保证拖拽元素不超出画布边界，则不要超出画布减去拖拽元素本身宽高的距离
            if (left < 124) {
                left = 124;
            } else if (left > body.clientWidth - newDivAddDrag.offsetWidth) {
                left = body.clientWidth - newDivAddDrag.offsetWidth / 2;
            }
            if (top < 0) {
                top = 0;
            } else if (top > body.clientHeight - newDivAddDrag.offsetHeight) {
                top = body.clientHeight - newDivAddDrag.offsetHeight;
            }
            // 实时给元素定位赋值
            newDivAddDrag.style.left = left + "px";
            newDivAddDrag.style.top = top + "px";
        }
        document.onmouseup = function () {
            this.onmousemove = null;
            this.onmouseup = null;
        }
    }
    //  操作代码
    for (let i = 0; i < nodeList.length; i++) {
        if (nodeList[i].innerText.indexOf('章') > -1 || nodeList[i].innerText.indexOf('综合复习题') > -1) {
            noUseCount++
        } else {
            useNodeList.push(nodeList[i])
            let p = document.createElement('p')
            p.classList.add("zd_study_p")
            p.innerText = nodeList[i].innerText
            p.style.whiteSpace = "nowrap"
            p.style.overflow = "hidden"
            p.style.textOverflow = "ellipsis"
            p.style.cursor = "pointer"
            p.style.padding = "5px"
            contentDiv.appendChild(p)
        }
    }
    newDivAdd.appendChild(contentDiv)
    document.body.appendChild(newDivAdd)
    myTitleH3.innerText = `共计：${useNodeList.length} 门课程(习题：${noUseCount} 个)，加载中...`
    timer = setInterval(() => {
        let videoContent = document.getElementById('tcvideoplayer_html5_api')
        let ZdStudyP = document.querySelectorAll('.zd_study_p')
        if (index >= useNodeList.length) {
            clearInterval(timer)
            timer = null
            myTitleH3.innerText = `！！！学习完成！！！`
            const studyPList = document.getElementById('studyPList');
            for (let i = 0; i < studyPList.children.length; i++) {
                const element = studyPList.children[i];
                element.remove();
            }
            studyPList.innerHTML = "<h1 style='text-align: center;'>恭喜你，学习完成</h1>"
            return
        }
        if (videoContent.paused) {
            ZdStudyP[index == -1 ? 0 : index].style.backgroundColor = "#fff"
            index++
            useNodeList[index].click()
            ZdStudyP[index].style.backgroundColor = "#f0f0f5"
            setTimeout(() => {
                let playBtn = document.querySelector(".vjs-big-play-button")
                playBtn.click()
                localStorage.setItem('index', index)
                myTitleH3.innerText = `正在学习 ${useNodeList[index].innerText}`
                setTimeout(()=>{
                    if(videoContent.muted == false){
                        videoContent.muted = true
                    }
                },2000)
                console.log("开始播放");
            }, 3000)
            console.warn(`准备播放 ${useNodeList[index].innerText}`);
        } else {
            console.warn(`当前正在播放 ${useNodeList[index].innerText}`);
        }
    }, 8000)
})()
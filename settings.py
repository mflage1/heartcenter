# -*- coding: utf-8 -*-
"""
File Name：     settings
Description :
date：          2024/2/23 023
"""
import os

# X_API_KEY = "f4ff2170f6af491aa8a7cb796a61ba14.70e34d07d130777d"
X_API_KEY = os.environ.get("X_API_KEY", "f0267098dbb8486086a536b1bd350d0a.62ff226183129a73")

DATABASES_USERNAME = os.environ.get("DATABASES_USERNAME", "heart")
DATABASES_PASSWORD = os.environ.get("DATABASES_PASSWORD", "mongo_heart")
DATABASES_HOST = os.environ.get("DATABASES_HOST", "127.0.0.1")
DATABASES_PORT = os.environ.get("DATABASES_PORT", 27017)
DATABASES_NAME = os.environ.get("DATABASES_NAME", "heart")
DATABASES_AUTHSource = os.environ.get("DATABASES_AUTHSource", "heart")

PROXY = None
